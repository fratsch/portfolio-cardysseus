﻿using UnityEngine;

public class FlipCamera : MonoBehaviour
{
	[SerializeField] private GameObject player;

	private bool camAngle = true;

	private void Update()
	{
		if (Input.GetButtonDown("Button B"))
		{
			camAngle = !camAngle;
		}

		if (camAngle)
		{
			transform.position = new Vector3(0.0f, 0.0f, -10.0f) + player.transform.position;
			transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
		}
		else
		{
			transform.position = new Vector3(0.0f, 20.0f, -10.0f) + player.transform.position;
			transform.rotation = Quaternion.Euler(60.0f, 0.0f, 0.0f);
		}
	}
}