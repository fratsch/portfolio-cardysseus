﻿// code written together with Sebastian Krause and Tim Sugaipov

using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure; // Required in C#
using UnityEngine.UI;
using Random = UnityEngine.Random;

public struct Player
{
    public GameObject CurrentObstacle;
    public Vector3 CursorPosition;
    public PlayerIndex PlayerIndex;
    public int Team;
    public int Score;
    public int Health;
}

public class PlayerController : MonoBehaviour
{
    private GamePadState _state;
    private GamePadState _prevState;
    public GameObject[] Items;
    private StreetGenerator _streetGenerator;
    private PlayerManager _manager;
    private int _currentDriverIndex;
    private Player[] _players = new Player[4];
    public int scoreAmount;
    public int scorePenalty;
    public GameObject cityCreator;
    private GoalGenerator _goalGenerator;

    [SerializeField] private GameObject camera;

    // Use this for initialization
    private void Start()
    {
        _manager = FindObjectOfType<PlayerManager>();
        _goalGenerator = cityCreator.GetComponent<GoalGenerator>();

        List<int> players = _manager.GetPlayerOrder();
        
        for (int i = 0; i < 4; i++)
        {
            Player player = _players[i];
            player.PlayerIndex = (PlayerIndex) i;
            player.CursorPosition = new Vector3();
            player.CurrentObstacle = Items[Random.Range(0, Items.Length - 1)];
            player.Health = 1;
            player.Score = 0;
            player.Team = Random.Range(0, players.Count);
            players.Remove(player.Team);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Confirm_P1") && _currentDriverIndex != 1)
        {
            Interact(_currentDriverIndex - 1);
        }
        
        if (Input.GetButtonDown("Confirm_P2") && _currentDriverIndex != 2)
        {
            Interact(_currentDriverIndex - 1);
        }
        
        if (Input.GetButtonDown("Confirm_P3") && _currentDriverIndex != 3)
        {
            Interact(_currentDriverIndex - 1);
        }
        
        if (Input.GetButtonDown("Confirm_P4") && _currentDriverIndex != 4)
        {
            Interact(_currentDriverIndex - 1);
        }
    }
    
    private void FixedUpdate()
    {
        Driving();
    }
    
    private void Driving()
    {
        float[] angleBetweenGoal = new float[2];
        angleBetweenGoal[0] = Vector3.Angle(transform.forward, _goalGenerator.goals[0].transform.position);
        angleBetweenGoal[1] = Vector3.Angle(transform.forward, _goalGenerator.goals[1].transform.position);
        int team = _players[_currentDriverIndex - 1].Team;
        
        for (int i = 0; i < 4; i++) {
            if (angleBetweenGoal[team] > 90f)
            {
                GamePad.SetVibration(_players[i].PlayerIndex, 1, 1);
            }
            else if (angleBetweenGoal[team] < 90f && angleBetweenGoal[team] > 45f)
            {
                GamePad.SetVibration(_players[i].PlayerIndex, 1.5f, 1.5f);
            }
            else
            {
                GamePad.SetVibration(_players[i].PlayerIndex, 2, 2);
                if (_players[i].Team == team)
                {
                    _players[i].Score += scoreAmount;
                }
            }
        }
    }
    
    private void Interact(int playerIndex)
    {
        Vector3 cursorPos = _players[playerIndex].CursorPosition;
        cursorPos.z = 10;
        cursorPos = Camera.main.ScreenToWorldPoint(cursorPos);
        Debug.Log(string.Format("{0}", cursorPos));
        Vector3 cameraPos = camera.transform.position;
        if (Physics.Raycast(cameraPos, (cursorPos - cameraPos).normalized, out var hitInfo, Mathf.Infinity,
            LayerMask.GetMask("Ground")))
        {
            Instantiate(_players[playerIndex].CurrentObstacle, new Vector3(hitInfo.point.x, 1, hitInfo.point.z), Quaternion.identity);
            _players[playerIndex].CurrentObstacle = Items[Random.Range(0, Items.Length - 1)];
        }
    }

    public void SetCurrentDriver(int driverIndex)
    {
        _currentDriverIndex = driverIndex;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Obstacle"))
        {
            _players[_currentDriverIndex - 1].Score -= scorePenalty;
            _players[_currentDriverIndex - 1].Health -= 1;
            if (_players[_currentDriverIndex - 1].Health <= 0)
            {
                _manager.NextDriver();
                Debug.Log("DriverSwitch");
            }
        }
    }
}
