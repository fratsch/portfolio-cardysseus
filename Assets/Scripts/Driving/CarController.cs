﻿// code regarding input has been written by Sebastian Krause

using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(Suspension))]
public class CarController : MonoBehaviour
{
	[SerializeField] private float acceleration = 25.0f;
	[SerializeField] private float breakForce = 10.0f;
	[SerializeField] private float maxSpeed = 20.0f;

	[SerializeField] private AnimationCurve turningCurve;

	private Rigidbody rb;
	private Suspension suspension;

	private string inputAccelerate;
	private string inputBreaking;
	private string inputTurning;

	private int currentDriverIndex;

	[Tooltip("Visualize Helper for CoM")] [SerializeField]
	private Transform coMViz;

	[SerializeField] Vector3 defaultCoM = new Vector3(0, -4, 0);
	[SerializeField] Vector3 forwardCoMShift = new Vector3(0, 0, -3);
	[SerializeField] Vector3 backwardCoMShift = new Vector3(0, 0, 3);

	[SerializeField] float minCoMShift = 0.0F;
	[SerializeField] float maxCoMShift = 3.0F;

	[SerializeField] private MeshRenderer[] PlayerColorDependantRenderers;

	private void Start()
	{
		rb = GetComponent<Rigidbody>();
		suspension = GetComponent<Suspension>();
	}

	public void SetDriver(int driverIndex, Color color)
	{
		currentDriverIndex = driverIndex;

		foreach (var mesh in PlayerColorDependantRenderers)
		{
			mesh.material.color = color;
		}

		inputAccelerate = "Acceleration_P" + driverIndex;
		inputBreaking = "Breaking_P" + driverIndex;
		inputTurning = "Horizontal_P" + driverIndex;
	}

	private void FixedUpdate()
	{
		for (int i = 0; i < 4; i++)
		{
			rb.AddForceAtPosition(suspension.CompressionForce[i], suspension.SuspensionPoints[i]);
		}

		Transform trans = transform;
		Vector3 position = trans.position;
		Vector3 forward = trans.forward;

		if (rb.velocity.magnitude < maxSpeed)
		{
			float accel = Input.GetAxisRaw(inputAccelerate);
		#if UNITY_EDITOR
			accel += Input.GetAxis("Acceleration"); // used for keyboard controls (debugging)
		#endif
			rb.AddForceAtPosition(Vector3.ProjectOnPlane(accel * acceleration * forward, Vector3.up),
				position + new Vector3(0.0f, -0.3f, 0.0f) + 0.5f * forward);

			float breaking = Input.GetAxisRaw(inputBreaking);
		#if UNITY_EDITOR
			breaking += Input.GetAxis("Breaking"); // used for keyboard controls (debugging)
		#endif
			rb.AddForceAtPosition(Vector3.ProjectOnPlane(breaking * breakForce * -forward, Vector3.up),
				position + new Vector3(0.0f, -0.3f, 0.0f) + 0.5f * forward);
		}

		bool bBackward = Vector3.Dot(transform.forward, rb.velocity) < 0F;
		rb.centerOfMass = Vector3.Lerp(defaultCoM, bBackward ? backwardCoMShift : forwardCoMShift,
			Mathf.Clamp01(bBackward ? 0.1F : (rb.velocity.magnitude / maxSpeed)));
		if (coMViz)
		{
			coMViz.position = rb.worldCenterOfMass;
		}

		float turn = Input.GetAxis(inputTurning);
	#if UNITY_EDITOR
		turn += Input.GetAxis("Horizontal"); // used for keyboard controls (debugging)
	#endif

		rb.AddTorque((bBackward ? -turn : turn) *
					 turningCurve.Evaluate(bBackward
						 ? ((rb.velocity.magnitude * 4F) / maxSpeed)
						 : (rb.velocity.magnitude * 2F / maxSpeed)) * transform.up);
	}
}