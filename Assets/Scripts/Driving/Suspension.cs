﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Suspension : MonoBehaviour
{
	private RaycastHit[] HitPoints { get; } = new RaycastHit[4];
	public Vector3[] SuspensionPoints { get; } = new Vector3[4];
	private float[] CompressionRatio { get; } = new float[4];
	public Vector3[] CompressionForce { get; } = new Vector3[4];

	private BoxCollider boxCollider;

	[SerializeField] private float suspensionLength = 1.0f;
	[SerializeField] private float suspensionForce = 1.0f;
	[SerializeField] private LayerMask layerMask;
	[SerializeField] private float wheelDistance = 0.2f;
	[SerializeField] private float wheelPosition = 0.35f;

	[Tooltip("index 0 to 3 is FL, FR, BL, BR")] [SerializeField]
	private GameObject[] wheels = new GameObject[4];

	private void Awake()
	{
		boxCollider = GetComponent<BoxCollider>();
	}

	private bool CalculateHitPoints(int i)
	{
		return Physics.Raycast(SuspensionPoints[i], -transform.up, out HitPoints[i], suspensionLength, layerMask);
	}

	private void FixedUpdate()
	{
		Vector3 size = boxCollider.size;

		SuspensionPoints[0] =
			transform.TransformPoint(boxCollider.center +
									 new Vector3(size.x, -size.y, size.z - wheelPosition - 0.2f) * 0.5f);
		SuspensionPoints[1] =
			transform.TransformPoint(boxCollider.center +
									 new Vector3(-size.x, -size.y, size.z - wheelPosition - 0.2f) * 0.5f);
		SuspensionPoints[2] =
			transform.TransformPoint(boxCollider.center + new Vector3(size.x, -size.y, -size.z + wheelPosition) * 0.5f);
		SuspensionPoints[3] =
			transform.TransformPoint(boxCollider.center +
									 new Vector3(-size.x, -size.y, -size.z + wheelPosition) * 0.5f);

		for (int i = 0; i < 4; i++)
		{
			Vector3 position;
			Vector3 transUp = transform.up;
			if (CalculateHitPoints(i))
			{
				CompressionRatio[i] = 1.0f - (SuspensionPoints[i] - HitPoints[i].point).magnitude / suspensionLength;
				CompressionForce[i] = CompressionRatio[i] * suspensionForce * transform.up;
				position = HitPoints[i].point + transUp * wheelDistance;
			}
			else
			{
				CompressionRatio[i] = 0.0f;
				CompressionForce[i] = Vector3.zero;
				position = SuspensionPoints[i] - transUp * (suspensionLength - wheelDistance);
			}

			wheels[i].transform.position = position;
		}
	}

	// for debugging
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;

		boxCollider = GetComponent<BoxCollider>();
		Vector3 size = boxCollider.size;

		Gizmos.DrawSphere(
			transform.TransformPoint(boxCollider.center +
									 new Vector3(size.x, -size.y, size.z - wheelPosition - 0.2f) * 0.5f), 0.1f);
		Gizmos.DrawSphere(
			transform.TransformPoint(boxCollider.center +
									 new Vector3(-size.x, -size.y, size.z - wheelPosition - 0.2f) * 0.5f), 0.1f);
		Gizmos.DrawSphere(
			transform.TransformPoint(boxCollider.center + new Vector3(size.x, -size.y, -size.z + wheelPosition) * 0.5f),
			0.1f);
		Gizmos.DrawSphere(
			transform.TransformPoint(boxCollider.center +
									 new Vector3(-size.x, -size.y, -size.z + wheelPosition) * 0.5f), 0.1f);
	}
}