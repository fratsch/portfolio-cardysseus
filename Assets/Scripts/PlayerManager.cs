﻿// code written together with Sebastian Krause and Tim Sugaipov

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [SerializeField] private CarController carController;
    [SerializeField] private PlayerController playerController;
    
    private List<int> PlayerOrder = new List<int>(new int[]{1,2,3,4});
    private int CurrentPlayerIndex = 0;
    readonly public Color[] PlayerColors = new Color[4] { Color.red, Color.cyan, Color.yellow, new Color(148F/255, 0, 211F/255) };
    // Start is called before the first frame update
    void Start()
    {
        PlayerOrder = PlayerOrder.OrderBy(a => Guid.NewGuid()).ToList();
        NextDriver();
    }

    public List<int> GetPlayerOrder()
    {
        return new List<int>(PlayerOrder);
    }
    
    public void NextDriver()
    {
        CurrentPlayerIndex = PlayerOrder[0];
        PlayerOrder = PlayerOrder.Skip(1).ToList();
        PlayerOrder.Add(CurrentPlayerIndex);
        
        carController.SetDriver(CurrentPlayerIndex, PlayerColors[CurrentPlayerIndex - 1]);
        playerController.SetCurrentDriver(CurrentPlayerIndex);
    }

    void Update() {
        if(Input.GetKeyUp(KeyCode.K)) {
            NextDriver();
        }
    }

    public bool IsPlayerTheDriver(int PlayerId) {
        return CurrentPlayerIndex == PlayerId;
    }
}
