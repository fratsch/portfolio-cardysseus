# Portfolio - Cardysseus

This game jam project was created as part of the indie schooltrip game jam in sweden.

The team consisted of three programmers.

The game was planned to be a four player game where two teams of two players had to wrestle for control of one car and drive it through a street maze to their secret
destination (indicated by the controller rumbling). The driver had to dodge obstacles placed by the other players. The roll of driver switches on a crash.

Unfortunately the project wasn't finished due to one team member leaving and the resulting lack of time.

My main tasks included the driving and vehicle mechanics which consist of input, movement and suspension simulation. The movement should be snappy
arcady and cartoony, hence the simulated suspensions.

Only files corresponding to these tasks are included.

Files which are needed for context but have not been written by me are marked by a comment in line one.